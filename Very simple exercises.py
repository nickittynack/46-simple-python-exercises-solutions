import string


# Exercise 1
def max(x, y):
    if x < y:
        return y
    else:
        return x


# Exercise 2
def max_of_three(x, y, z):
    return max(x, max(y, z))


# Exercise 3
def length(o):
    count = 0
    for i in o:
        count += 1
    return count


# Exercise 4
def is_vowel(l):
    return l.lower() in 'aeiou'


# Exercise 5
def translate(s):
    phrase = ""
    for letter in s:
        if letter.upper() in 'BCDFGHJKLMNPQRSTVXZWY':
            phrase += letter + "o"
        phrase += letter
    return phrase


# Exercise 6
def sum(l):
    total = 0
    for e in l:
        total += e
    return total


def multiply(l):
    total = 1
    for e in l:
        total *= e
    return total


# Exercise 7
def reverse(s):
    backwards = ""
    for e in s:
        backwards = e + backwards
    return backwards


# Exercise 8
def is_palindrome(s):
    return s == s[::-1]


# Exercise 9
def is_member(x, a):
    for i in a:
        if i is x:
            return True
    return False


# Exercise 10
def overlapping(x, y):
    for e_x in x:
        for e_y in y:
            if e_y is e_x:
                return True
    return False


# Exercise 11
def generate_n_chars(n, c):
    s = ""
    for i in range(0, n):
        s += c
    return s


# Exercise 12
def histogram(l):
    for i in l:
        print(generate_n_chars(i, "*"))


# Exercise 13
def max_in_list(l):
    current_max = -1
    for i in l:
        if current_max < i:
            current_max = i
    return current_max


# Exercise 14
def map_word_length(l):
    lengths = []
    for word in l:
        lengths.append(len(word))
    return lengths


# Exercise 15
def find_longest_word(l):
    longest = ""
    for word in l:
        if len(longest) < len(word):
            longest = word
    return longest


# Exercise 16
def filter_long_words(l, n):
    longest = []
    for word in l:
        if len(word) > n:
            longest.append(word)
    return longest


# Exercise 17
def is_palindrome_smarter(s):
    symbols = list(string.punctuation) + list(string.whitespace)
    for symbol in symbols:
        s = s.replace(symbol, '')
    return is_palindrome(s.lower())


# Exercise 18
def is_pangram(s):
    letters = list(s)
    alphabet = list(string.lowercase)
    for l in letters:
        if l.lower() in alphabet:
            alphabet.remove(l.lower())
    return not alphabet


# Exercise 19
def sing_bottles_of_beer(b):
    print("%s bottles of beer on the wall, %s bottles of beer. Take one down, pass it around, %s bottles of beer on the wall." % (b, b, b - 1))
    b -= 1
    if b > 0:
        sing_bottles_of_beer(b)


# Exercise 20
def translate(l):
    swedish = {
        "merry": "god",
        "christmas": "jul",
        "and": "och",
        "happy": "gott",
        "new": "nytt",
        "year": "ar"
    }
    translated = []
    for word in l:
        w = word
        if word in swedish:
            w = swedish[word]
        translated.append(w)
    return translated


# Exercise 21
def char_freq(s):
    freq = {}
    for letter in s:
        w = letter.lower()
        if w in freq:
            freq[w] += 1
        else:
            freq[w] = 1
    return freq


# Exercise 22
def caesar_cipher(s):
    cipher = {
        'a': 'n', 'b': 'o', 'c': 'p', 'd': 'q', 'e': 'r', 'f': 's', 'g': 't', 'h': 'u',
        'i': 'v', 'j': 'w', 'k': 'x', 'l': 'y', 'm': 'z', 'n': 'a', 'o': 'b', 'p': 'c',
        'q': 'd', 'r': 'e', 's': 'f', 't': 'g', 'u': 'h', 'v': 'i', 'w': 'j', 'x': 'k',
        'y': 'l', 'z': 'm', 'A': 'N', 'B': 'O', 'C': 'P', 'D': 'Q', 'E': 'R', 'F': 'S',
        'G': 'T', 'H': 'U', 'I': 'V', 'J': 'W', 'K': 'X', 'L': 'Y', 'M': 'Z', 'N': 'A',
        'O': 'B', 'P': 'C', 'Q': 'D', 'R': 'E', 'S': 'F', 'T': 'G', 'U': 'H', 'V': 'I',
        'W': 'J', 'X': 'K', 'Y': 'L', 'Z': 'M'
    }
    result = ""
    for letter in s:
        if letter in cipher:
            result += cipher[letter]
        else:
            result += letter
    return result


# Exercise 23
def correct(s):
    corrected = ' '.join(s.split())  # Reduce whitespace
    corrected = '. '.join(corrected.split('.'))  # Spaces after fullstops
    print(corrected.strip())


# Exercise 24
def make_3sg_form(s):
    if s[-1] is 'y':
        return s[:-1] + 'ies'
    elif s[-1] in 'osxz' or s[-2:] in {'sh', 'ch'}:
        return s + 'es'
    return s + 's'


# Exercise 25
def make_ing_form(s):
    if s[-1] is 'e' and s not in {'be', 'see', 'flee', 'knee'}:
        return s[:-1] + 'ing'
    elif s[-2:] is 'ie':
        return s[-2:] + 'ying'
    elif len(s) is 3 and not is_vowel(s[0]) and is_vowel(s[1]) and not is_vowel(s[2]):
        return s + s[-1] + 'ing'
    return s + 'ing'